<?php

$config['dec'] = "Dic";
$config['nov'] = "Nov";
$config['oct'] = "Oct";
$config['sep'] = "Sep";
$config['ago'] = "Ago";
$config['jul'] = "Jul";
$config['jun'] = "Jun";
$config['may'] = "May";
$config['apr'] = "Abr";
$config['mar'] = "Mar";
$config['feb'] = "Feb";
$config['jan'] = "Ene";

$config['manage_boxs_lang'] = "Gestionar tipo de cajas";
$config['listar_boxs_lang'] = "Listar cajas";
$config['add_box_lang'] = "Adicionar tipo de caja";
$config['update_box_lang'] = "Actualizar Tipo de caja";

$config['manage_measures_lang'] = "Gestionar medidas y pesos";
$config['listar_measures_lang'] = "Listar medidas y pesos";
$config['add_measure_lang'] = "Adicionar medida|peso";
$config['update_measure_lang'] = "Actualizar medida|peso";

$config['manage_countrys_lang'] = "Gestionar paises de entrega";
$config['listar_countrys_lang'] = "Listar paises de entrega";
$config['add_country_lang'] = "Adicionar país de entrega";
$config['update_country_lang'] = "Actualizar país de entrega";

$config['stems_bunch_lang'] = "Stems bunch";

$config['me_dicen_lang'] = "Me dicen";
$config['reception_pedido_lang'] = "Recepción";
$config['description_pedido_lang'] = "Descripción del pedido";
$config['comensales_lang'] = "Comensales";
$config['description_menu_lang'] = "Descripción del menú";
$config['category_lang'] = "Categoría";
$config['users_lang'] = "Usuarios";
$config['manage_users'] = "Gestionar usuarios";
$config['manage_categories_lang'] = "Gestionar categorías para menús";
$config['listar_categoria_lang'] = "Listar categorías";
$config['manaage_category_lang'] = "Gestionar categoría";
$config['image_lang'] = "Imagen";
$config['update_category_lang'] = "Editar categoría";
$config['pizarra_resumen_lang'] = "Pizarra resumen";
$config['not_allow_extension'] = "La extensión no está permitida";
$config['data_saved_ok'] = "Los datos se han guardado correctamente";
$config['administrador_lang'] = "Administrador";
$config['usuario_activo_lang'] = "Usuario activo";
$config['usuario_inactivo_lang'] = "Usuario inactivo";
$config['desactivar_user_lang'] = "Desactivar usuario";
$config['activar_usuario_lang'] = "Activar usuario";
$config['operation_made_ok_lang'] = "La operación se realizó correctamente";
$config['manage_users_lang'] = "Gestionar usuario";
$config['user_list_lang'] = "Lista de usuarios";
$config['add_user_lang'] = "Adicionar usuario";
$config['repeat_password_lang'] = "Repetir contraseña";
$config['edit_user_lang'] = "Editar usuario";
$config['mis_menus_lang'] = "Mis menús";
$config['manage_menu_lang'] = "Gestionar menú";
$config['listar_menus_lang'] = "Listar menús";
$config['mis_menus_activos_lang'] = "Mis menús activos";
$config['menus_desactivados_lang'] = "Menús desactivados";
$config['add_menu_lang'] = "Adicionar menú";
$config['description_lang'] = "Descripción";
$config['description'] = "Descripción";
$config['menu_active_lang'] = "Menú activo";
$config['no_lang'] = "No";
$config['yes_lang'] = "Si";
$config['revisar_comentarios_lang'] = "Revisar comentarios";
$config['desactivar_menu_lang'] = "Desactivar menú";
$config['lista_menus_desactivados_lang'] = "Lista de menús desactivados";
$config['activar_menu_lang'] = "Activar menú";
$config['maximo_menu_actives_lang'] = "No se puede activar el menú porque el máximo de menús activos es 3";
$config['category_menus_lang'] = "Categoría de Menú";
$config['category_platos_lang'] = "Categoría de Platos";
$config['usuario_lang'] = "Usuario";
$config['not_defined_lang'] = "No definido";
$config['pagina_web_lang'] = "Página Web";
$config['me_defino_lang'] = "Me defino";
$config['ofrezco_servicio_en'] = "Ofrezco servicio en";
$config['los_servicios_que_ofrezco_son'] = "Los servicios que ofrezco son:";
$config['realizar_oferta_lang'] = "Realizar oferta";
$config['description_servicio_lang'] = "Descripción del servicio";
$config['ver_detalle_lang'] = "Ver detalle";
$config['ver_conversacion_lang'] = "Ver conversación";
$config['detalle_solicitud_lang'] = "Detalle de solicitud";
$config['lista_solicitudes_lang'] = "Lista de solicitudes";
$config['msg_required_lang'] = "Su mensaje no puede estar en blanco";
$config['msg_sended_ok'] = "Mensaje enviado correctamente";
$config['oferta_no_enviada_lang'] = "Oferta no enviada";
$config['oferta_enviada_lang'] = "Oferta enviada";
$config['state_lang'] = "Estado";
$config['pendiente_aprobacion_lang'] = "Por aprobar";
$config['producto_aprobado_lang'] = "Pedido Procesado";
$config['oferta_aceptada_lang'] = "Oferta aceptada";
$config['oferta_rechazada_lang'] = "Oferta rechazada";
$config['solicitud_confirmada_lang'] = "Solicitud confirmada";
$config['solicitud_sin_confirmar_lang'] = "Solicitud sin confirmar";
$config['cant_visitas_lang'] = "Cantidad de visitas";
$config['ingresos_lang'] = "Ingresos";
$config['manejo_ingresos_lang'] = "Manejo de ingresos";
$config['lista_ingresos_lang'] = "Lista de ingresos";
$config['codigo_pedido_lang'] = "Código del pedido";
$config['total_venta_lang'] = "Total de la venta";
$config['total_lang'] = "Total por Cantidad ";
$config['monto_oferta_lang'] = "Monto de la venta";
$config['monto_pagar_chef_lang'] = "Pagar al Chef";
$config['monto_ganancia_chef_lang'] = "Monto de la ganancia del Chef";
$config['monto_ganancia_cliente_lang'] = "Monto de la ganancia cliente";
$config['resumen_ingresos_lang'] = "Resumen de ingresos";
$config['nuestros_chefs_lang'] = "Nuestros Chefs";
$config['menus_lang'] = "Menús";
$config['select_tu_experiencia_lang'] = "Selecciona tu experiencia";
$config['tendencia_lang'] = "Tendencia";
$config['ubicacion_lang'] = "Ubicación";
$config['que_buscas_lang'] = "¿ Qué buscas... ?";
$config['buscar_lang'] = "Buscar";
$config['sorprendeme_lang'] = "Sorpréndeme";
$config['experiencia_lang'] = "Experiencia";
$config['addres_lang'] = "Dirección";
$config['credenciales_lang'] = "Credenciales";

$config['manage_photos_lang'] = "Gestionar fotos";
$config['listar_photos_lang'] = "Listar fotos";
$config['go_to_menus_lang'] = "Ir a los menús";
$config['add_photo_lang'] = "Adicionar foto";
$config['update_photo_lang'] = "Editar foto";
$config['galeria_fotos_lang'] = "Galería de fotos";
$config['fotos_platos_lang'] = "Fotos de los platos";
$config['lista_platos_lang'] = "Lista de platos";
$config['formulario_lang'] = "Formulario";
$config['subir_imagen_lang'] = "Subir imagen";
$config['listar_comentarios_lang'] = "Listar comentarios";
$config['date_lang'] = "Fecha";
$config['date_pedido_lang'] = "Fecha Pedido";
$config['comentario_lang'] = "Comentario";
$config['evaluacion_lang'] = "Evaluación";
$config['plato_lang'] = "Plato";
$config['mi_perfil_lang'] = "Mi perfil";
$config['mis_pedidos_lang'] = "Mis pedidos";
$config['mi_perfil_personal_lang'] = "Mi perfil personal";

$config['datos_cliente_lang'] = "Datos del Cliente";
$config['login_error'] = "Error de autenticación";
$config['dashboard_lang'] = "Pizarra resumen";
$config['cliente_lang'] = "Cliente";
$config['inicio_lang'] = "Inicio";
$config['mas_info_lang'] = "Más información";
$config['cuentas_lang'] = "Cuentas";
$config['super_admin_lang'] = "Súper Admin";
$config['profile_lang'] = "Perfil de usuario";
$config['sign_out_lang']  = "Cerrar sesión";
$config['email_lang'] = "Email";
$config['password_lang'] = "Contraseña";
$config['entrar_lang'] = "Entrar";
$config['manage_cuentas_lang'] = "Gestionar cuentas";
$config['listar_cuentas_lang'] = "Listar cuentas";
$config['add_item_lang'] = "Adicionar elemento";
$config['nombre_lang'] = "Nombre";
$config['nombre_pedido_lang'] = "Nombre del Pedido";
$config['tipo_gestion_lang'] = "Tipo gestión";
$config['monto_lang'] = "Monto";
$config['actions_lang'] = "Acciones";
$config['credito_lang'] = "Crédito";
$config['debito_lang'] = "Débito";
$config['manual_lang'] = "Manual";
$config['automatica_lang'] = "Automática";
$config['movimientos_lang'] = "Movimientos";
$config['add_cuenta_lang'] = "Adicionar cuenta";
$config['tipo_cuenta_lang'] = "Tipo de cuenta";
$config['cuenta_lang'] = "Cuenta";
$config['guardar_info_lang'] = "Guardar información";
$config['valor_tc_not_valid_lang'] = "El valor recibido en el tipo de de cuenta no es válido";
$config['valor_tg_not_valid_lang'] = "El valor recibido en el tipo de de gestión no es válido";
$config['data_saved_ok'] = "Los datos han sido guardados satisfactoriamente";
$config['edit_lang'] = "Editar";
$config['delete_lang'] = "Eliminar";
$config['update_cuenta_lang'] = "Editar cuenta";
$config['data_deleted_ok'] = "Los datos han sido eliminados satisfactoriamente";
$config['presupuesto_lang'] = "Presupuesto";
$config['manage_presupuesto_general_lang'] = "Gestionar el presupuesto general";
$config['listar_presupuestos_generales_lang'] = "Listar presupuestos generales";
$config['fecha_lang'] = "Fecha";
$config['add_presupuesto_general_lang'] = "Adicionar presupuesto general";
$config['year_lang'] = "Año";
$config['mes_lang'] = "Mes";
$config['valor_mes_not_valid_lang'] = "El valor del mes no es válido";
$config['mes_year_already_exist'] = "El presupuesto general para el mes y año especificado ya existe";
$config['presupuesto_fraccionado_lang'] = "Presupuesto fraccionado";
$config['gestion_presupuesto_fraccionado_lang'] = "Gestión del presupuesto fraccionado";
$config['listar_presupuestos_fraccionado_lang'] = "Listado de presupuesto fraccionados";
$config['fecha_maxima_lang'] = "Fecha máxima";
$config['estado_lang'] = "Estado";
$config['back_lang'] = "Atrás";
$config['add_presupuesto_fraccionado_lang'] = "Adicionar presupuesto fraccionado";
$config['fecha_maxima_pago_lang'] = "Fecha máxima de pagos";
$config['tipo_presupuesto_lang'] = "Tipo de presupuesto";
$config['fijo_lang'] = "Fijo";
$config['variable_lang'] = "Variable";
$config['valor_tipo_presupuesto_not_valid_lang'] = "El valor del tipo de presupuesto no es correcto";
$config['monto_sobrepasa_presupuesto_general_lang'] = "El monto sobrepasa el presupuesto general asociado";
$config['update_presupuesto_fraccionado_lang'] = "Editar presupuesto fraccionado";
$config['data_deleted_ok'] = "Los datos han sido eliminados correctamente";
$config['operaciones_lang'] = "Operaciones";
$config['manage_operaciones_lang'] = "Gestionar operaciones";
$config['listar_operaciones_lang'] = "Listar operaciones";
$config['descripcion_lang'] = "Descripción";
$config['concepto_lang'] = "Concepto";
$config['manage_concepto_lang'] = "Gestionar conceptos";
$config['listar_mis_conceptos_lang'] = "Listar mis conceptos";
$config['add_concepto_lang'] = "Adicionar concepto";
$config['update_concepto_lang'] = "Actualizar concepto";
$config['concepto_con_operaciones_lang'] = "No se puede eliminar el concepto porque tiene operaciones asociadas";
$config['add_operacion_lang'] = "Adicionar operación";
$config['lista_operaciones_lang'] = "Listar operaciones";
$config['description_lang'] = "Descripción";
$config['tipo_operacion_lang'] = "Tipo de operación";
$config['ingreso_lang']  = "Ingreso";
$config['egreso_lang'] = "Egreso";
$config['date_lang'] = "Fecha";
$config['valor_tipo_operacion_not_valid_lang'] = "El valor del tipo de operación no es válida";
$config["concepto_not_valid"] = "El concepto no es válido";
$config['update_operacion_lang'] = "Editar operación";
$config['datos_chef_lang'] = "Datos Chef";
$config['cambiar_password_lang'] = "Cambiar contraseña";
$config['ciudades_trabajo_lang'] = "Ciudades donde trabajo";
$config['asi_me_presento_lang'] = "Así me presento";
$config['mis_principales_ingredientes_lang'] = "Mis principales ingredientes";
$config['conoceme_rapido_lang'] = "Conóceme rápido";
$config['mis_redes_sociales_lang'] = "Mis redes sociales";
$config['mis_solicitudes_lang'] = "Mis solicitudes";
$config['precio_por_comensal_lang'] = "Precio por comensal";
$config['calificaciones_lang'] = "CALIFICACION(ES)";
$config['tendencias_busquedas_lang'] = "Tendencias de búsquedas";
$config['pedidos_recibidos_lang'] = "Pedidos recibidos";
$config['pedidos_recibidos_lang'] = "Pedidos recibidos";
$config['evaluacion_lang'] = "Evaluación";
$config['comentarios_recibidos_lang'] = "Comentarios recibidos";
$config['precio_promedio_comensal_lang'] = "Precio promedio por comensal";
$config['visitas_perfil_lang'] = "Visitas a mi perfil";
$config['cantidad_pedidos_lang'] = "Cantidad de pedidos";
$config['cantidad_lang'] = "Cantidad";
$config['ciudades_ofrezco_servicio_lang'] = "Lista de ciudades donde ofrezco mi servicio";
$config['ciudad_lang'] = "Ciudad";
$config['add_ciudad_lang'] = "Adicionar ciudad";
$config['ciudad_ya_seleccionada_lang'] = "La ciudad que intenta agregar ya se encuentra seleccionada";
$config['como_me_presento_lang'] = "Como me presento";
$config['servicios_ofrecidos_lang'] = "Los servicios que ofrezco";
$config['hablanos_tu_experiencia_lang'] = "Háblanos de tu experiencia";
$config['guardar_mi_perfil_lang'] = "Guardar mi perfil";
$config['must_select_a_service_lang'] = "Usted debe seleccionar al menos un servicio para ofrecer";
$config['los_ingredientes_usados_lang'] = "Los ingredientes que siempre uso";
$config['ingrediente_lang'] = "Ingrediente";
$config['add_ingrediente_lang'] = "Adicionar ingrediente";
$config['conoceme_rapido_lang'] = "Conóceme rápido";
$config['por_que_chef_domicilio_lang'] = "¿ Por qué eres chef a domicilio ?";
$config['donde_aprendi_cocinar_lang'] = "¿ Donde aprendiste a cocinar ?";
$config['cual_referente_cocina_lang'] = "¿ Cuál es tu referente en la cocina ?";
$config['algun_Secreto_cocina_lang'] = "¿Algún secreto en la cocina que quieras compartir?";
$config['guardar_info_lang'] = "Guardar información";
$config['mis_redes_sociales_lang'] = "Mis redes sociales";
$config['change_password_lang'] = "Cambiar contraseña";
$config['nueva_contrasena_lang'] = "Nueva contraseña";
$config['repeat_password_lang'] = "Repetir contraseña";
$config['visitas_lang'] = " visitas";
$config['login_register_lang'] = "Autenticación";
$config['about_us_lang'] = "Sobre nosotros";
$config['vinculos_ayuda_lang'] = "Vínculos de ayuda";
$config['entrar_lang'] = "Entrar";
$config['me_gusta_que_me_llamen_lang'] = "Me gusta que me llamen";


$config['esta_pagado_lang'] = "Está pagado ?";
$config['not_gasto_sobrepasa_presupuesto'] = "No puede registrar el gasto porque sobrepasa el presupuesto definido";
$config['not_gasto_sobrepasa_cuenta'] = "No se puede registrar el gasto porque excede el monto disponible en cuentas";
$config['definete_en_una_frase_lang'] = "Defínete en una frase";
$config['si_tienes_web_ponla_aqui_lang'] = "Si tienes alguna web ponla aquí";
$config['me_defino_como_lang'] = "Me defino como";

$config['cupo_lang'] = "Cupo";
$config['monto_cupo_lang'] = "El valor del monto no puede superar el valor del cupo";
$config['pagar_tarjeta_credito'] = "Pagar tarjeta de crédito";
$config['phone_lang'] = "Teléfono";
$config['password_not_match'] = "Las contraseñas no coinciden";
$config['email_already_exist_lang'] = "El email está siendo usado por otro usuario";
$config['user_register_success'] = "El usuario se ha registrado correctamente";
$config['pagar_tarjeta_lang'] = "Pagar tarjetas";
$config['pagar_tarjeta_lang'] = "Pagar tarjeta";
$config['deuda_tarjeta'] = "Deuda de la tarjeta";
$config['not_fondos'] = "La cuenta no tiene fondos para realizar la transacción";
$config['tarjeta_pagada_ok'] = "La tarjeta fue pagada correctamente";
$config['pedidos_lang'] = "Pedidos";
$config['datos_orden_lang'] = "Datos de la orden ";
$config['password_change_ok_lang'] = "La contraseña ha sido cambiada correctamente";
$config['pagos_por_aprobar_lang'] = "Pagos por aprobar";
$config['soy_lang'] = "Soy";

$config['manipular_solicitudes_lang'] = "Manipular solicitudes";
$config['listar_solicitudes_lang'] = "Listar solicitudes";
$config['fecha_hora_lang'] = "Fecha y Hora";
$config['fecha_servicio_lang'] = "Fecha del servicio";
$config['hora_servicio_lang'] = "Hora del servicio";
$config['conversacion_lang'] = "Conversación";
$config['direct_conversation_lang'] = "Conversación directa";
$config['escriba_msg_lang'] = "Escriba su mensaje...";
$config['enviar_mensaje_lang'] = "Enviar mensaje ahora";
$config['referencia_lang'] = "Referencia";
$config['manipular_platos_menu_lang'] = "Manipular platos del menú";
$config['lista_clientes_lang'] = "Lista de clientes";













//Form validation
$config['required'] = "El campo %s no puede estar vacío";
$config['min_length'] = "El mínimo de caracteres en %s es %s";
$config['max_length'] = "El máximo de caracteres de %s es %s";
$config['valid_email'] = "El correo electrónico %s no es válido";
$config['matches'] = "El campo %s no coincide con %s";
$config['is_unique'] = "El valor %s está siendo utilizado";
$config['trim'] = "El campo %s esta vacío";
$config['numeric'] = "El campo %s debe contener un valor numérico";
$config['exact_length'] = "El campo %s debe contener %s catacteres";
$config['greater_than'] = "El campo %s debe ser mayor de %s";
$config['less_than'] = "El campo %s debe ser menor de %s";
$config['alpha'] = "El campo %s solo puede contener caracteres alfabéticos";
$config['alpha_numeric'] = "El campo %s solo puede contener caracteres alfa-numéricos";
$config['alpha_dash'] = "El campo %s solo puede contener caracteres alfabéticos y '-'";
$config['integer'] = "El campo %s debe contener un valor entero";
$config['decimal'] = "El campo %s debe contener un valor entero";
$config['is_natural'] = "El campo %s debe contener un valor natural";
$config['is_natural_no_zero'] = "El campo %s debe contener un valor natural mayor a cero";
$config['valid_emails'] = "Los correos electrónicos no son vÃ¡lidos";
$config['valid_ip'] = "El valor %s no es un IP válido";
$config['valid_base64'] = "%s no es un valor base64 válido";
$config['alpha_numeric_space'] = "El campo %s solo puede contener caracteres alfanuméricos y espacios";
$config['valid_url'] = "El campo %s del servicio debe ser una URL válida";


$config['facebook_lang'] = "Facebook";
$config['twitter_lang'] = "Twitter";
$config['youtube_lang'] = "Youtube";

$config['palabras_dinam_lang'] = "Palabras Dinámicas";
$config['palabras_lang'] = "Palabras Claves";
$config['proyectos_lang'] = "Proyectos";
$config['clientes_lang'] = "Clientes";
$config['desarrolladores_lang'] = "Desarrolladores";
$config['premios_lang'] = "Premios";

$config['login_app'] = "Autenticarse en la aplicación";
$config['email_lang'] = "E-mail";
$config['password_lang'] = "Contraseña";
$config['or_lang'] = "O";
$config['login_using_face'] = "Autenticar usando Facebook";
$config['login_using_google'] = "Autenticar usando Google+";
$config['i_forgot_password_lang'] = "Yo olvidé mi contraseña";
$config['registrarse_lang'] = "Registrarse";
$config['login_error'] = "Error de autenticación";
$config['entrar_lang'] = "Entrar";
$config['pizarra_resumen_lang'] = "Pizarra resumen";
$config['panel_lang'] = "Panel Principal";
$config['manage_users'] = "Gestionar usuarios";

$config['ancho_lang'] = "Ancho";
$config['alto_lang'] = "Alto";
$config['doble_hoja_lang'] = "Doble Hoja";

$config['cerrar_lang'] = "Cerrar";
$config['numero_lang'] = "Número";
$config['numero_pedido_lang'] = "Nro Pedido";

$config['cargo_lang'] = "Cargo";
$config['message_lang'] = "Mensaje";
$config['manage_mensajes_lang'] = "Gestionar mensajes";
$config['listar_mensajes_lang'] = "Listar mensajes";

$config['foto_area_lang'] = "Foto del área";
$config['numero_identidad_lang'] = "Número de identidad";
$config['direccion_lang'] = "Dirección";
$config['state_lang'] = "Estado";
$config['featured_lang'] = "Destacado";
$config['caracteristicas_lang'] = "Características";
$config['icon_lang'] = "Ícono";

$config['data_update_ok'] = "Los datos se guardaron correctamente";

$config['manage_caracts_lang'] = "Gestionar Características";
$config['listar_caracts_lang'] = "Listar Características";
$config['add_caracts_lang'] = "Adicionar Características";

$config['comment_lang'] = "Comentario";
$config['comments_lang'] = "Comentarios";
$config['manage_comments_lang'] = "Gestionar Comentarios";
$config['listar_comments_lang'] = "Listar Comentarios";
$config['change_lang'] = "Cambiar Estado";
$config['usuario_lang'] = "Usuario";

$config['tipo_lang'] = "Tipo";
$config['asign_lang'] = "Asignación";
$config['book_lang'] = "Libro";
$config['books_lang'] = "Libros";
$config['manage_books_lang'] = "Gestionar Libros";
$config['manage_book_lang'] = "Gestionar Libro";
$config['listar_books_lang'] = "Listar Libros";
$config['editar_books_lang'] = "Editar Libro";
$config['ver_book_lang'] = "Ver Libro";
$config['ver_lang'] = "Ver";
$config['add_book_lang'] = "Adicionar Libro";
$config['cod_lang'] = "Código Libro";
$config['edicion_lang'] = "Edición Libro";
$config['fecha_salida_lang'] = "Fecha Salida";
$config['resumen_lang'] = "Resumen";
$config['precio_lang'] = "Precio";
$config['add_pages'] = "Adicionar Páginas";
$config['manage_pages_lang'] = "Gestionar Páginas";
$config['pages_lang'] = "Páginas";
$config['page_lang'] = "Página";

$config['archivo_lang'] = "Archivo";
$config['paquete_lang'] = "Paquete";
$config['paquetes_lang'] = "paquetes";
$config['manage_paquete_lang'] = "Gestionar Paquetes";
$config['listar_paquete_lang'] = "Listar Paquetes";
$config['editar_paquete_lang'] = "Editar Paquete";
$config['add_paquete_lang'] = "Adicionar Paquete";
$config['add_descripcion_lang'] = "Descripción";

$config['client_lang'] = "Cliente";

$config['manage_clients_lang'] = "Gestionar clientes";
$config['listar_clients_lang'] = "Listar clientes";
$config['editar_client_lang'] = "Editar cliente";
$config['add_client_lang'] = "Adicionar cliente";

$config['cod_lang'] = "Código";
$config['cods_lang'] = "Códigos";
$config['manage_cod_lang'] = "Gestionar Códigos";
$config['listar_cod_lang'] = "Listar Códigos";
$config['editar_cod_lang'] = "Editar Código";
$config['add_cod_lang'] = "Adicionar Código";
$config['descripcion_corta_lang'] = "Descripción Corta";
$config['descripcion_larga_lang'] = "Descripción Larga";
$config['date_inicio_lang'] = "Fecha Inicio";
$config['date_fin_lang'] = "Fecha Fin";
$config['date_salida_lang'] = "Fecha Salida";

$config['asunto_lang'] = "Tema";

$config['manage_users_lang'] = "Gestión de usuarios";
$config['admin_caj_sup_lang'] = "Administradores";
$config['add_item_lang'] = "Adicionar elemento";
$config['user_list_lang'] = "Lista de usuarios";
$config['fullname_lang'] = "Nombre completo";
$config['phone_lang'] = "Teléfono";
$config['date_lang'] = "Fecha";
$config['actions_lang'] = "Acciones";
$config['edit_lang'] = "Editar";
$config['delete_lang'] = "Eliminar";
$config['back_lang'] = "Atrás";
$config['add_user_lang'] = "Adicionar usuario";
$config['phone_lang'] = "Teléfono";
$config['cedula_lang'] = "Cédula";
$config['address_lang'] = "Dirección";
$config['password_lang'] = "Contraseña";
$config['repeat_password_lang'] = "Repetir contraseña";
$config['administrador_lang'] = "Administrador";
$config['editor_lang'] = "Editor";
$config['supervisor_lang'] = "Supervisor";
$config['data_saved_ok'] = "Los datos se guardaron correctamente";
$config['data_changed_ok'] = "El estado se cambió correctamente";
$config['guardar_info_lang'] = "Guardar información";
$config['role_lang'] = "Rol de usuario";
$config['data_deleted_ok'] = "El elemento ha sido eliminado correctamente";

$config['date_inicio_salida_lang'] = "Rango de fechas";

$config['manage_banners_lang'] = "Gestionar banners";
$config['listar_banners_lang'] = "Listar banners";
$config['banners_lang'] = "Banners";
$config['banner_lang'] = "Banner";
$config['add_banner_lang'] = "Adicionar banner";
$config['update_banner_lang'] = "Editar banner";
$config['text_lang'] = "Texto";
$config['url_lang'] = "Url";

$config['manage_services_lang'] = "Gestionar servicios";
$config['listar_services_lang'] = "Listar servicios";
$config['services_lang'] = "Servicios";
$config['service_lang'] = "Servicio";
$config['add_service_lang'] = "Adicionar servicio";
$config['update_service_lang'] = "Editar servicio";

$config['manage_productos_lang'] = "Gestionar variedades";
$config['listar_productos_lang'] = "Listar variedades";
$config['productos_lang'] = "Variedades";
$config['producto_lang'] = "Variedad";
$config['add_producto_lang'] = "Adicionar variedad";
$config['update_producto_lang'] = "Editar variedad";

$config['manage_noticias_lang'] = "Gestionar noticias";
$config['listar_noticias_lang'] = "Listar noticias";
$config['noticias_lang'] = "Noticias";
$config['noticia_lang'] = "Noticia";
$config['titulo_lang'] = "Título";
$config['cuerpo_lang'] = "Cuerpo";
$config['presentacion_lang'] = "Presentación";
$config['add_noticia_lang'] = "Adicionar noticia";
$config['update_noticia_lang'] = "Editar noticia";

$config['manage_empresa_lang'] = "Gestionar Empresa";
$config['empresa_lang'] = "Empresa";
$config['update_empresa_lang'] = "Editar Empresa";
$config['video_lang'] = "Video";
$config['mision_lang'] = "Misión";
$config['vision_lang'] = "Visión";
$config['horario_lang'] = "Horario";

$config['manage_seguimiento_lang'] = "Gestionar seguimientos";
$config['listar_seguimiento_lang'] = "Listar seguimientos";
$config['add_seguimiento_lang'] = "Adicionar seguimiento";
$config['update_seguimiento_lang'] = "Editar seguimiento";
$config['seguimiento_lang'] = "Seguimiento";


$config['manage_categories_lang'] = "Gestionar categorías";
$config['listar_categoria_lang'] = "Listar categorías";
$config['nombre_lang'] = "Nombre";
$config['nombre'] = "Nombre";
$config['add_category_lang'] = "Adicionar categoría";
$config['categories_lang'] = "Categorías";
$config['update_category_lang'] = "Editar categoría";
$config['manage_tipo_evento_lang'] = "Gestionar tipos de eventos";
$config['listar_tipos_eventos'] = "Listar tipos de eventos";
$config['add_tipo_evento_lang'] = "Adicionar tipo de eventos";
$config['update_tipo_evento'] = "Editar tipo de evento";
$config['escenarios_lang'] = "Escenarios";

$config['manage_escenario_lang'] = "Gestionar escenarios";
$config['image_lang'] = "Imagen";
$config['description_lang'] = "Descripción";
$config['capacidad_lang'] = "Capacidad";
$config['add_escenario_lang'] = "Adicionar escenario";
$config['update_escenario_lang'] = "Editar datos del escenario";
$config['feature_escenario_lang'] = "Características";
$config['manage_features_lang'] = "Gestionar características del escenario";
$config['add_area_lang'] = "Adicionar área";
$config['area_padre'] = "AREA PADRE";
$config['no_defined_lang'] = "No definida";

$config['add_area_lang'] = "Adicionar área";
$config['tipo_area_lang'] = "Tipo de Área";
$config['area_con_asientos'] = "Área con asientos";
$config['area_con_proposito_libre_lang'] = "Área de propósito libre";
$config['list_subareas'] = "Lista de sub áreas";
$config['update_area_lang'] = "Editar área";
$config['list_asientos'] = "Lista de asientos";

$config['add_asiento_lang'] = "Adicionar asiento";
$config['lista_asientos_lang'] = "Lista de asientos";
$config['manage_eventos_lang'] = "Gestionar eventos";
$config['fecha_evento_lang'] = "Fecha del evento";
$config['gallery_lang'] = "Galería";
$config['manage_gallery_lang'] = "Gestionar galería";
$config['add_evento_lang'] = "Evento";

$config['hora_lang'] = "Hora";
$config['personas_lang'] = "Personas";
$config['update_evento_lang'] = "Editar evento";
$config['manage_image_evento'] = "Gestionar imágenes de evento";
$config['add_image_lang'] = "Adicionar imagen";

$config['galeria_principal_lang'] = "Galería principal";
$config['manage_gallery_items_lang'] = "Gestionar elementos de galería";
$config['listar_elementos_galeria'] = "Listar elementos de la galería";

$config['evento_lang'] = "Evento";
$config['form_item_gallery_lang'] = "Formulario de elementos para galería";
$config['gallery_image_lang'] = "Imagen para galería";
$config['evento_asociado_lang'] = "Evento asociado";
$config['realizar_oparation_lang'] = "Realizar operación";
$config['escenario_lang'] = "Escenario";
$config['category_lang'] = "Categoría";
$config['tipo_evento_lang'] = "Tipo de evento";
$config['profile_lang'] = "Datos de perfil";
$config['sign_out_lang'] = "Cerrar sesión";
$config['youtube_url_lang'] = "URL de Youtube";
$config['tabla_precios_lang'] = "Tabla de precios";

$config['manage_tabla_precios'] = "Gestionar tabla de precios";
$config['localidad_lang'] = "Localidad";
$config['price_lang'] = "Precio";
$config['caracteristica_lang'] = "Característica";
$config['capacidad_lang'] = "Capacidad";
$config['add_item_tabla_lang'] = "Adicionar elemento tabla de precio";

$config['listar_log'] = "Listar Logs";
$config['description_lang'] = "Descripción";
$config['action_lang'] = "Acción";
$config['model_lang'] = "Modelo";
$config['idModel_lang'] = "id Modelo";
$config['field_lang'] = "Campo";
$config['creationdate_lang'] = "Fecha";
$config['userid_lang'] = "Usuario";

$config['update_tabla_precios_lang'] = "Editar tabla de precios";
$config['configuration_lang'] = "Configuración";
$config['areas_escenario_lang'] = "Configuración de escenario";
$config['establecer_precio_lang'] = "Establecer precio";
$config['asignacion_precio_lang'] = "Asignación de precio";

$config['establecer_precio'] = "Establecer precio";
$config['add_asiento_lang'] = "Adicionar asiento";
$config['update_asiento_lang'] = "Editar asiento";
$config['manage_puntos_venta'] = "Gestionar puntos de venta";
$config['listar_puntos_venta_lang'] = "Listar puntos de venta";
$config['form_punto_venta'] = "Formulario para puntos de venta";
$config['pto'] = "Punto de venta";
$config['guardar_posicion'] = "Guardar posición";
$config['lat_lang'] = "Latitud";
$config['long_lang'] = "Longitud";
$config['mapa_lang'] = "Mapa";
$config['posicion_seleccionada'] = "Posición";

$config['contacto_lang'] = "Contacto";
