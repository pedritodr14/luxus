<section id="content">
    <div class="container">
        <div class="content-pages">
            <div class="bread-crumb">

            </div>
            <div class="about-intro text-center">
                <h2 class="title18">Ranic Sport</h2>
                <h2 class="title30 text-center">Información</h2>
                <p class="desc">Implementos deportivos de alta calidad.</p>
            </div>
            <!-- End Intro -->
            <div class="about-service">
                <div class="row">
                    <div class="col-md-4 col-sm-4 col-xs-12">
                        <div class="item-about-service text-center">
                            <div class="about-service-icon">
                                <a href="#"><i class="fa fa-address-book-o" aria-hidden="true"></i></a>
                            </div>
                            <h3 class="title18"><a href="#" class="black">Sobre nosotros</a></h3>
                            <?= $empresa_object->about ?>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-4 col-xs-12">
                        <div class="item-about-service text-center">
                            <div class="about-service-icon">
                                <a href="#"><i class="fa fa-list-alt" aria-hidden="true"></i></a>
                            </div>
                            <h3 class="title18"><a href="#" class="black">Mision</a></h3>
                            <?= $empresa_object->mision ?>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-4 col-xs-12">
                        <div class="item-about-service text-center">
                            <div class="about-service-icon">
                                <a href="#"><i class="fa fa-line-chart" aria-hidden="true"></i></a>
                            </div>
                            <h3 class="title18"><a href="#" class="black">Vision</a></h3>
                            <?= $empresa_object->vision ?>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</section>