<section id="content">
    <div class="container">
        <div class="bread-crumb">
            <a href="#" class="silver">Home</a><a href="#" class="silver">Men’s </a><span class="color">Tennis</span>
        </div>
        <div class="content-pages">
            <div class="row">
                <div class="col-md-3 col-sm-4 col-xs-12">
                    <div class="sidebar-left sidebar-shop">
                        <div class="widget widget-category-icon">
                            <h2 class="title-widget title18">Categorias</h2>
                            <div class="list-cat-icon2">
                                <ul class="list-none">
                                    <li><a href="#"><img src="images/icons/cat1.png" alt="">Bikes Ovicsport store <span class="silver">(9)</span></a></li>
                                    <li><a href="#"><img src="images/icons/cat2.png" alt="">Basketball Sport store<span class="silver">(32)</span></a></li>
                                    <li><a href="#"><img src="images/icons/cat3.png" alt="">Weightlifting Ovicsport <span class="silver">(19)</span></a></li>
                                    <li><a href="#"><img src="images/icons/cat4.png" alt="">Volleyball Store <span class="silver">(26)</span></a></li>
                                    <li><a href="#"><img src="images/icons/cat5.png" alt="">Football Ovicsport <span class="silver">(5)</span></a></li>
                                    <li><a href="#"><img src="images/icons/cat6.png" alt="">Golf Ovicsport store <span class="silver">(9)</span></a></li>
                                    <li><a href="#"><img src="images/icons/cat8.png" alt="">Rowing store <span class="silver">(4)</span></a></li>
                                    <li><a href="#"><img src="images/icons/cat9.png" alt="">Runner moving<span class="silver">(7)</span></a></li>
                                </ul>
                            </div>
                        </div>

                    </div>
                </div>
                <div class="col-md-9 col-sm-8 col-xs-12">
                    <div class="content-shop shop-grid">
                        <div class="shop-title-box">
                            <h2 class="title18 title-box5">Men’s</h2>
                            <!--   <div class="view-type">
                                <a href="grid.html" class="grid-view"></a>
                                <a href="list.html" class="list-view active"></a>
                            </div> -->
                        </div>
                        <!--   <div class="shop-banner banner-adv line-scale">
                            <a href="#" class="adv-thumb-link"><img src="images/shop/banner.jpg" alt="" /></a>
                        </div> -->
                        <div class="list-shop-product">
                            <div class="item-product item-product-list">
                                <div class="row">
                                    <div class="col-md-4 col-sm-5 col-xs-5">
                                        <div class="product-thumb">
                                            <span class="product-label sale-label">50% off</span>
                                            <a href="detail.html" class="product-thumb-link">
                                                <img src="images/photos/sport_10.jpg" alt="">
                                            </a>
                                        </div>
                                    </div>
                                    <div class="col-md-8 col-sm-7 col-xs-7">
                                        <div class="product-info">
                                            <h3 class="product-title title14"><a href="detail.html">Sport product Name</a></h3>
                                            <div class="product-price">
                                                <del><span class="title14 silver">$798.00</span></del>
                                                <ins><span class="title14 color">$399.00</span></ins>
                                            </div>
                                            <div class="product-rate">
                                                <div class="product-rating" style="width:100%"></div>
                                            </div>
                                            <span class="inout-stock in-stock"><i class="fa fa-check-square" aria-hidden="true"></i>In stock</span>
                                            <p class="desc">100% cotton double printed dress. Black and white striped top and orange high waisted skater skirt bottom. Duis lobortis dui lacus egeterat </p>
                                            <div class="product-extra-link">
                                                <a href="#" class="addcart-link">Add to cart</a>
                                                <a href="#" class="wishlist-link"></a>
                                                <a href="#" class="compare-link"></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="item-product item-product-list">
                                <div class="row">
                                    <div class="col-md-4 col-sm-5 col-xs-5">
                                        <div class="product-thumb">
                                            <span class="product-label sale-label">50% off</span>
                                            <a href="detail.html" class="product-thumb-link">
                                                <img src="images/photos/sport_11.jpg" alt="">
                                            </a>
                                        </div>
                                    </div>
                                    <div class="col-md-8 col-sm-7 col-xs-7">
                                        <div class="product-info">
                                            <h3 class="product-title title14"><a href="detail.html">Sport product Name</a></h3>
                                            <div class="product-price">
                                                <del><span class="title14 silver">$798.00</span></del>
                                                <ins><span class="title14 color">$399.00</span></ins>
                                            </div>
                                            <div class="product-rate">
                                                <div class="product-rating" style="width:100%"></div>
                                            </div>
                                            <span class="inout-stock in-stock"><i class="fa fa-check-square" aria-hidden="true"></i>In stock</span>
                                            <p class="desc">100% cotton double printed dress. Black and white striped top and orange high waisted skater skirt bottom. Duis lobortis dui lacus egeterat </p>
                                            <div class="product-extra-link">
                                                <a href="#" class="addcart-link">Add to cart</a>
                                                <a href="#" class="wishlist-link"></a>
                                                <a href="#" class="compare-link"></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="item-product item-product-list">
                                <div class="row">
                                    <div class="col-md-4 col-sm-5 col-xs-5">
                                        <div class="product-thumb">
                                            <span class="product-label sale-label">50% off</span>
                                            <a href="detail.html" class="product-thumb-link">
                                                <img src="images/photos/sport_12.jpg" alt="">
                                            </a>
                                        </div>
                                    </div>
                                    <div class="col-md-8 col-sm-7 col-xs-7">
                                        <div class="product-info">
                                            <h3 class="product-title title14"><a href="detail.html">Sport product Name</a></h3>
                                            <div class="product-price">
                                                <del><span class="title14 silver">$798.00</span></del>
                                                <ins><span class="title14 color">$399.00</span></ins>
                                            </div>
                                            <div class="product-rate">
                                                <div class="product-rating" style="width:100%"></div>
                                            </div>
                                            <span class="inout-stock in-stock"><i class="fa fa-check-square" aria-hidden="true"></i>In stock</span>
                                            <p class="desc">100% cotton double printed dress. Black and white striped top and orange high waisted skater skirt bottom. Duis lobortis dui lacus egeterat </p>
                                            <div class="product-extra-link">
                                                <a href="#" class="addcart-link">Add to cart</a>
                                                <a href="#" class="wishlist-link"></a>
                                                <a href="#" class="compare-link"></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="item-product item-product-list">
                                <div class="row">
                                    <div class="col-md-4 col-sm-5 col-xs-5">
                                        <div class="product-thumb">
                                            <span class="product-label sale-label">50% off</span>
                                            <a href="detail.html" class="product-thumb-link">
                                                <img src="images/photos/sport_13.jpg" alt="">
                                            </a>
                                        </div>
                                    </div>
                                    <div class="col-md-8 col-sm-7 col-xs-7">
                                        <div class="product-info">
                                            <h3 class="product-title title14"><a href="detail.html">Sport product Name</a></h3>
                                            <div class="product-price">
                                                <del><span class="title14 silver">$798.00</span></del>
                                                <ins><span class="title14 color">$399.00</span></ins>
                                            </div>
                                            <div class="product-rate">
                                                <div class="product-rating" style="width:100%"></div>
                                            </div>
                                            <span class="inout-stock in-stock"><i class="fa fa-check-square" aria-hidden="true"></i>In stock</span>
                                            <p class="desc">100% cotton double printed dress. Black and white striped top and orange high waisted skater skirt bottom. Duis lobortis dui lacus egeterat </p>
                                            <div class="product-extra-link">
                                                <a href="#" class="addcart-link">Add to cart</a>
                                                <a href="#" class="wishlist-link"></a>
                                                <a href="#" class="compare-link"></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="sort-paginav pull-right">
                            <div class="pagi-bar">
                                <a href="#" class="current-page">1</a>
                                <a href="#">2</a>
                                <a href="#">3</a>
                                <a href="#" class="next-page">next <i class="fa fa-angle-double-right" aria-hidden="true"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--    <div class="list-service">
            <div class="row">
                <div class="col-md-4 col-sm-4 col-xs-12">
                    <ul class="item-service list-inline-block">
                        <li>
                            <div class="service-icon">
                                <a href="#"><img class="wobble-horizontal" src="images/home/home1/form1.png" alt="" /></a>
                            </div>
                        </li>
                        <li>
                            <div class="service-info">
                                <h3 class="title18 font-bold"><a href="#" class="black">Order Online</a></h3>
                                <h4 class="title14 transition">Hours: 8AM -11PM</h4>
                            </div>
                        </li>
                    </ul>
                </div>
                <div class="col-md-4 col-sm-4 col-xs-12">
                    <ul class="item-service list-inline-block item-active active">
                        <li>
                            <div class="service-icon">
                                <a href="#"><img class="wobble-horizontal" src="images/home/home1/form2.png" alt="" /></a>
                            </div>
                        </li>
                        <li>
                            <div class="service-info">
                                <h3 class="title18 font-bold"><a href="#" class="black">Save 30% </a></h3>
                                <h4 class="title14 transition">When you use credit card</h4>
                            </div>
                        </li>
                    </ul>
                </div>
                <div class="col-md-4 col-sm-4 col-xs-12">
                    <ul class="item-service list-inline-block">
                        <li>
                            <div class="service-icon">
                                <a href="#"><img class="wobble-horizontal" src="images/home/home1/form3.png" alt="" /></a>
                            </div>
                        </li>
                        <li>
                            <div class="service-info">
                                <h3 class="title18 font-bold"><a href="#" class="black">Free Shipping</a></h3>
                                <h4 class="title14 transition">On orders over $99</h4>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div> -->
        <!-- End List Service -->

    </div>
</section>